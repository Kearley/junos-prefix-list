'''
!/usr/bin/python3

Purpose:
    Configure/edit a specific prefix list on a Junos devices.  Does the following:
    - Sources an IP prefix list file (./input)
    - Renders them into a Junos config in a staging area (./staging)
    - Pushes and commits the configuration as a change to the Junos device(s) - optional
    - Records the applied changes as a diff (./audit)

Author: 
    dkearley@integrationpartners.com

Dependencies:
- Junos PyEZ (junos-exnc)
- netaddr
- PyYaml
- jinja2
- getpass
- argparse
- datetime

Example:

dkearley@wally% python3 provision.py -d 11.232.104.10 11.232.104.11 -u ipcadmin -f my_prefixes.yaml


'''


#################
# Imports 
#################
from jnpr.junos import Device
from jnpr.junos.utils.config import Config
import jnpr.junos.exception
import getpass, argparse
import ipaddress
from jinja2 import Environment, FileSystemLoader, Template 
import os, yaml
import datetime, time
from pprint import pprint as pp


#################
# Globals 
#################
line = '*' * 80
audit_dir = 'audit/'
template = 'prefix.j2'
staging_dir = 'staging/'
config = 'config.txt'
scrubbed_prefixes = 'working_prefixes.yaml'
# Env = Environment(loader=FileSystemLoader(searchpath=template_dir))


#################
# Functions 
#################
def get_args():
    '''
    Specify and parse parameters from user with argparse().
    Returns the parsed arguments as a dictionary.
    '''
    parser = argparse.ArgumentParser(description="Provision an IP prefix list on a Junos device(s).")
    parser.add_argument('-d', '--device', nargs='+', help='The hostname or ip address of the device or \
        list of devices (separated by a space) that you wish to provision.')
    parser.add_argument('-f', '--file', help='The filename containing your IP Prefixes you wish to provision.')
    parser.add_argument('-u', '--user', help='Junos admin username to use for these device connections.  \
        Defaults to current logged in localhost user.', default=getpass.getuser())
    args = parser.parse_args()
    return(args)

def validate_prefixes(file):
    '''
    Takes a file of IPv4 prefixes and parse for valid and invalid prefixes.  
    Save both prefix types to a list and return as a single dictionary. 
    Dictionary is dumped to a working yaml file if user wishes to review.
    '''

    # Create empty data structures
    prefix_dict = dict()
    prefix_list = list()
    invalid_ips = list()

    # Open the file, read prefixes, and validate each with ipaddress() and save to separate lists. 
    with open(file) as prefixes:
        prefix_data = prefixes.read().splitlines()
        for prefix in prefix_data:
            try: 
                ip_addr = ipaddress.IPv4Network(prefix)
                prefix_list.append(prefix)
            except:
                invalid_ips.append(prefix)

    # Check invalid IP list for entries and add to dictionary.  
    if len(invalid_ips) > 0:
        prefix_dict['ommitted'] = invalid_ips

    # Add good prefix list to the dictionary
    prefix_dict['prefixes'] = prefix_list

    # Dump the dictionary to working yaml file
    with open(staging_dir + scrubbed_prefixes, 'w') as yaml_file:
        yaml.dump(prefix_dict,yaml_file, explicit_start=True)
    
    # Print a user warning if invalid_ips[] is not empty.
    if len(invalid_ips) > 0:
        print('\nWARNING - The following are not valid IPv4 prefixes and will be ommitted:')
        for ip in invalid_ips:
            print('\t-{}'.format(ip))

    return(prefix_dict) 

def config_render(template, data):
    ''' 
    Renders device configurations using jinja2 templates. 
    Takes dictionary as input data and returns a configuration object. 
    '''
    my_template = Environment(loader=FileSystemLoader(searchpath='.')).get_template(template)
    rendered_config = my_template.render(data)

    # Debug
    # print(rendered_config)

    with open(staging_dir + config, 'w') as config_file:
        config_file.write(rendered_config)



def get_config(device, user, passwd):
    # Open a device connection 
    try:
        with Device(host=device, user=user, password=passwd) as dev:
            config = dev.rpc.get_config()
            hostname = dev.facts['hostname']
            return(config,hostname)
    except LockError:
        print('\nError: Configuration was locked!')
    except ConnectRefusedError:
        print('\nError: Device connection refused!')
    except ConnectTimeoutError:
        print('\nError: Device connection timed out!')
    except ConnectAuthError:
        print('\nError: Authentication failure!')
    except ConfigLoadError as err:
        print('\nError: ' + str(err))


def configure_device(devices,USER,PASSWD):
    ''' Set variables needed for timestamped files'''
    now = datetime.datetime.now()
    datets = now.strftime("%Y-%m-%d_T%H%M%S") 

    myconfig = staging_dir + config

    ''' Iterate over dictionary of devices in vrf yaml file and render the configs based on service_type defined 
    and make a copy of the changes in file/staging '''
    for device in devices: 
        # Connect to each device, configure and commit #
        print('Working on device: {}'.format(device))
        try:
            with Device(host=device,user=USER, password=PASSWD) as dev:
                # open and close is done automatically by context manager
                with Config(dev, mode='exclusive') as conf:
                    # exclusive locks are treated automatically by context manager
                    conf.load(path = myconfig, format='text')
                    diff = conf.diff()
                    if diff is None:
                        print('Configuration is up to date on device!  Nothing to do...\n')
                    else:
                        print('Pending changes to be committed:')
                        print(diff)
                        # save the configuration to timestamped file
                        with open(audit_dir + device + "_" + datets + ".txt" , "a") as diff_file:
                            diff_file.write(diff)
                        conf.commit() 
                        print('Configuration successfully committed and recorded to: {}\n'.format(diff_file.name))


        except LockError:
            print('\nError applying config: configuration was locked!')
        except ConnectRefusedError:
            print('\nError: Device connection refused!')
        except ConnectTimeoutError:
            print('\nError: Device connection timed out!')
        except ConnectAuthError:
            print('\nError: Authentication failure!')
        except ConfigLoadError as ex:
            print('\nError: ' + str(ex))


def main():
    # Collect user input and parse for device connections
    args = get_args()
    passwd = getpass.getpass()

    # Start the clock for fun
    test_start = time.time()

    # Validate and process the prefix_data from prefix file provided
    print(line)
    print('Parsing the prefix file: {}...'.format(args.file))
    prefix_dict = validate_prefixes(args.file)
    print('Parsing Complete!\n')

    # Debug
    # pp(prefix_dict)
    
    # Render and write configurations
    print(line)
    print('Rendering the new configuration as: {}...'.format(staging_dir + config))
    config_render(template,prefix_dict)
    print('Rendering Complete!\n')
    
    # Perform device configuration 
    print(line)
    print('Now performing device configurations...')
    # devices = args.device.split()
    configure_device(args.device,args.user,passwd)
    print('Configuration Complete!\n')

    print(line)
    print('\nTotal execution time:', time.time()-test_start,'seconds\n')
    print('Call it', round(time.time()-test_start,3),'seconds...\n')
    print(line)

#################
# Main 
#################
if __name__ == "__main__":
    main()
